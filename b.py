def add(x, y):
    """ Ajoute deux nombres.
    Exemple :
    >>> add(1, 3)
    4
    >>> add(0, 0)
    0
    """
    return x + y
